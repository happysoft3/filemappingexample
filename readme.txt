

공유 파일 업로딩/다운로딩 및 생성

업로딩 측.
1. SharedFile 객체 생성
  - 이때, 인자로 파일명을 넘겨줌

2. bool SharedFile::DoMapping(); 메서드 호출 시 공유 메모리에 파일 내용이 업로드 됨


다운로딩 측.
1. SharedFile 객체 생성

2. bool SharedFile::FetchFile(); 메서드 호출 시 공유 메모리 로부터 파일을 내려받음






