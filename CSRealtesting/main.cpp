﻿// CSRealtesting.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "iniFileMan.h"
#include <iostream>
#include <algorithm>
#include <string>

void Testing(char *str)
{
    printf("%p\n", str);
    std::cout << str << std::endl;
}

template <class Container>
inline auto toArray(Container &c)->decltype(&c[0])
{
    return c.data();
}

template <class Container>
inline auto toArray(const Container &cc)
->decltype(const_cast<Container::value_type*>(&cc[0]))
{
    return const_cast<Container::value_type*>(cc.data());
}

template <int N>
struct Factorial
{
    static constexpr int value = N * Factorial<N - 1>::value;
};

template <>
struct Factorial<1>
{
    static constexpr int value = 1;
};

template <int N>
struct Adder
{
    static constexpr int value = N + Adder<N-1>::value;
};

template <>
struct Adder<1>
{
    static constexpr int value = 1;
};

template <typename Container>
Container::value_type * ExtractArrayFromDataStruct(const Container &cont)
{
    using valueType = Container::value_type;
    size_t contLength = cont.size();
    valueType *result = new valueType[contLength];
    typename Container::const_iterator cIt = cont.cbegin();

    int indexRep = 0;
    while (cIt != cont.cend())
        result[indexRep++] = *(cIt++);
    return result;
}

template <class CharT>
std::basic_string<CharT> AdjectCharT(const std::basic_string<CharT> &src, CharT tLeft, CharT tRight)
{
    size_t srcLength = src.size();
    std::basic_string<CharT> tResult(srcLength + 2, 0);

    tResult[0] = tLeft;
    tResult[++srcLength] = tRight;
    std::copy(src.cbegin(), src.cend(), ++tResult.begin());
    
    return tResult;
}

template <class CharT>
std::basic_string<CharT> AdjectCharT(const std::basic_string<CharT> &src, CharT tBoth)
{
    return AdjectCharT(src, tBoth, tBoth);
}

int main()
{
    IniFileMan inifile;

    bool pResult = inifile.ReadIni("test.txt");

    std::string resstr = "no";

    inifile.GetItemValue("INSTALL", "SEND_COMPLETE", resstr);
    int res = -1;
    inifile.SetItemValue("INSTALL", "SEND_COMPLETE", 500);
    inifile.GetItemValue("INSTALL", "SEND_COMPLETE", res);

    inifile.WriteIni();

    std::cout << resstr << ", " << res << std::endl;

    std::getchar();
    return 0;
}
