﻿
/**
* upload 시 SHARED_FILE_UPLOADING 정의
* download 시 SHARED_FILE_UPLOADING 해제
* 속도 테스팅. 큰 파일일 땐 뭐가 빠르고. 작은건 뭐가 유리하고 등등
*/

#include "sharedFile.h"
#include "utils/printing.h"
#include "utils/runclock.h"

#define SHARED_FILE_UPLOADING

int main(int argc, char **argv)
{
	RunClock execClock;

#ifdef SHARED_FILE_UPLOADING
	if (argc != 2)
		return 0;

	SharedFile file(argv[1]);

	file.RegistAction([](std::shared_ptr<sharedFileCbData> data)
		{
			_printUtil::Print("filename: ", data->m_dataFilename, ", ", std::to_string(data->m_iDataFilesize), "bytes");
		}, SharedFile::callbackActions::OnMapped);

	bool pResult = file.DoMapping();

#else

	SharedFile file;

	file.RegistAction([](std::shared_ptr<sharedFileCbData> data)
		{
			_printUtil::Print("filename: ", data->m_dataFilename, ", ", std::to_string(data->m_iDataFilesize), "bytes");
		}, SharedFile::callbackActions::OnCreated);

	bool pResult = file.FetchFile();
	
#endif
	execClock.Show();

	_printUtil::Print("result ?= ", pResult ? "ok" : "fail");
	std::getchar();

	return 0;
}