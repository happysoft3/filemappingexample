
#ifndef RUN_CLOCK_H__
#define RUN_CLOCK_H__

class RunClock
{
private:
	double m_initClock;
	double m_latestClock;

public:
	explicit RunClock();
	~RunClock();
	void Show(bool reset = false);
	void Reset();
};

#endif

