
#ifndef PRINTING_H__
#define PRINTING_H__

#include <iostream>

namespace _printUtil
{
    template <class T>
    void Print(T arg)
    {
        std::cout << arg << std::endl;
    }

    template <class T, class... Types>
    void Print(T arg, Types... args)
    {
        std::cout << arg << static_cast<char>(0x20);
        Print(args...);
    }
}

#endif



