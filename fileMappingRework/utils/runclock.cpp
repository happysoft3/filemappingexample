
#include "runclock.h"
#include <time.h>
#include <iostream>
#include <string>

RunClock::RunClock()
{
	Reset();

	m_initClock = m_latestClock;
}

RunClock::~RunClock()
{
	Show();
}

void RunClock::Show(bool reset)
{
	double cClock = static_cast<double>(clock());

	std::cout << "end of process " + std::to_string((cClock - (reset ? m_latestClock : m_initClock)) / CLOCKS_PER_SEC) + " sec\n";
	if (reset)
		Reset();
}

void RunClock::Reset()
{
	clock_t currentClock = clock();

	m_latestClock = currentClock;
}