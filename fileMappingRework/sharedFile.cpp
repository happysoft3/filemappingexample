
/**
* upload: filename을 주면, head+file 합친 크기만큼 매핑
* download: 기존대로 head읽고 쓰면댐
*/

#include "sharedFile.h"
#include <algorithm>
#include "utils/printing.h"

constexpr uint32_t _sharedFileMagicNumber = 0xdeadface;
constexpr uint32_t _sharedFileIOBufferCapa = 10;

SharedFile::SharedFile(const std::string &filename)
{
	m_hFile = INVALID_HANDLE_VALUE;
	m_hMapping = nullptr;
	m_filename = filename;
	m_iFilesize = 0;
	m_pMappedPtr = nullptr;
	m_sharedmemId = "defaultName";

	m_onCreated = nullptr;
	m_onMapped = nullptr;
}

SharedFile::~SharedFile()
{
	reset();
	deinitialize();
}

bool SharedFile::isCompleted(bool bCond, sharedFileDeinitType deinit)
{
	if (bCond && deinit != nullptr)
		m_deinitList.push_back(deinit);

	return bCond;
}

bool SharedFile::deinitialize()
{
	while (m_deinitList.size())
	{
		(this->*(m_deinitList.back()))();
		m_deinitList.pop_back();
	}
	return true;
}

bool SharedFile::create(uint32_t accessmode, uint32_t dwCreateFlag)
{
	if (!m_filename.length())
		return false;

	reset();
	m_hFile = ::CreateFileA(m_filename.c_str(), accessmode, 0, nullptr, dwCreateFlag, FILE_ATTRIBUTE_NORMAL, nullptr);
	return isValidFileHandle();
}

bool SharedFile::reset()
{
	if (!isValidFileHandle())
		return false;

	::CloseHandle(m_hFile);
	m_hFile = INVALID_HANDLE_VALUE;
	return true;
}

bool SharedFile::filesize()
{
	if (!isValidFileHandle())
		return false;

	DWORD filesizeHigh = 0;
	m_iFilesize = ::GetFileSize(m_hFile, &filesizeHigh);

	if (!m_iFilesize)
		return false;

	return true;
}

std::string SharedFile::getOnlyFilename()
{
	uint32_t iMedSep = m_filename.find_last_of('\\');

	return m_filename.substr(iMedSep ? iMedSep + 1 : 0);
}

bool SharedFile::makeHeader()
{
	m_header = std::unique_ptr<sharedFileHeaderType>(new sharedFileHeaderType({}));

	m_header->m_iMagic = _sharedFileMagicNumber;
	m_header->m_iStreamLength = m_iFilesize;
	m_header->m_iHeaderLength = sizeof(sharedFileHeaderType);
	std::string filename = getOnlyFilename();
	std::copy_n(filename.begin(), filename.length() > _sharedFileNameLength ? _sharedFileNameLength : filename.length(), m_header->m_szFilename);
	
	return true;
}

bool SharedFile::writeHeader()
{
	if (m_pMappedPtr == nullptr)
		return false;
	if (!m_header)
		return false;

	byte* headerRaw = reinterpret_cast<byte*>(m_header.get());
	std::copy_n(headerRaw, m_header->m_iHeaderLength, m_pMappedPtr);

	return true;
}

bool SharedFile::writeStream()
{
	if (m_pMappedPtr == nullptr)
		return false;
	if (!m_header)
		return false;

	uint32_t progress = 0;

	m_percent = -1;
	do
	{
		if (!readFromFile(progress))
			return false;
	} while (dumping(progress));

	return true;
}

bool SharedFile::unsetFilesize()
{
	if (!m_iFilesize)
		return false;

	m_iFilesize = 0;
	return true;
}

bool SharedFile::deinitMapHandle()
{
	if (m_hMapping == nullptr)
		return false;

	::CloseHandle(m_hMapping);
	m_hMapping = nullptr;
	return true;
}

bool SharedFile::unmapping()
{
	if (m_pMappedPtr == nullptr)
		return false;

	::UnmapViewOfFile(m_pMappedPtr);
	return true;
}

bool SharedFile::readFromFile(uint32_t &progress)
{
	if (m_hFile == INVALID_HANDLE_VALUE)
		return false;

	uint32_t iCapa = ((m_iFilesize - progress) > _sharedFileIOBufferCapa) ? _sharedFileIOBufferCapa : (m_iFilesize - progress);

	m_filestream.resize(iCapa);

	if (!::ReadFile(m_hFile, m_filestream.data(), m_filestream.size(), nullptr, nullptr))
		return false;

	progress += iCapa;
	return true;
}

bool SharedFile::dumping(uint32_t &progress)
{
	if (progress >= m_iFilesize)
		return false;

	uint32_t iDumpbase = m_header->m_iHeaderLength + progress - m_filestream.size();
	
	std::copy_n(m_filestream.begin(), m_filestream.size(), m_pMappedPtr + iDumpbase);
	int iPerc = static_cast<int>(static_cast<float>(static_cast<float>(progress) / m_iFilesize) * 100);
	if (iPerc != m_percent)
	{
		m_percent = iPerc;
		_printUtil::Print(std::to_string(iPerc) + " %");
	}
	return true;
}

bool SharedFile::mapping()
{
	if (!create(GENERIC_READ | GENERIC_WRITE, OPEN_EXISTING))
		return false;

	if (!isCompleted(filesize(), &SharedFile::unsetFilesize))
		return false;

	uint32_t iMappedsize = 0;
	if (!makeHeader())
		return false;
	mappingSize(iMappedsize);

	m_hMapping = ::CreateFileMappingA(INVALID_HANDLE_VALUE, nullptr, PAGE_READWRITE, 0, iMappedsize, m_sharedmemId.c_str());
	if (!isCompleted(m_hMapping != nullptr, &SharedFile::deinitMapHandle))
		return false;
	byte* mappedptr = static_cast<byte*>(::MapViewOfFile(m_hMapping, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, iMappedsize));
	if (!isCompleted(mappedptr != nullptr, &SharedFile::unmapping))
		return false;
	m_pMappedPtr = mappedptr;
	if (!writeHeader())
		return false;
	if (!writeStream())
		return false;

	runInfoCallback(m_onMapped);

	return true;
}

void SharedFile::showMapped()
{
	auto mapp = m_pMappedPtr;
	
	if (mapp == nullptr || !m_header)
		return;
	int iLength = m_header->m_iHeaderLength;

	for (int u = 0; u < iLength; ++u)
		std::cout << std::hex << static_cast<int>(mapp[u]) << ' ';
}

bool SharedFile::loadMapping()
{
	m_hMapping = ::OpenFileMappingA(FILE_MAP_READ, false, m_sharedmemId.c_str());

	if (!isCompleted(m_hMapping != nullptr, &SharedFile::deinitMapHandle))
		return false;
	
	byte* mappedptr = static_cast<byte*>(::MapViewOfFile(m_hMapping, FILE_MAP_READ, 0, 0, 0));

	if (!isCompleted(mappedptr != nullptr, &SharedFile::unmapping))
		return false;

	m_pMappedPtr = mappedptr;
	return true;
}

bool SharedFile::checkHeader()
{
	if (m_pMappedPtr == nullptr)
		return false;

	sharedFileHeaderType* head = reinterpret_cast<sharedFileHeaderType*>(m_pMappedPtr);

	if (head->m_iMagic != _sharedFileMagicNumber)
		return false;
	m_iFilesize = head->m_iStreamLength;
	m_filename = head->m_szFilename;
	
	return makeHeader();
}

bool SharedFile::writeFromMemory()
{
	if (!checkHeader())
		return false;
	if (!create(GENERIC_WRITE, CREATE_ALWAYS))
		return false;

	byte* stream = m_pMappedPtr + m_header->m_iHeaderLength;
	uint32_t progress = 0, iBuffLength = (m_iFilesize - progress > _sharedFileIOBufferCapa) ? _sharedFileIOBufferCapa : (m_iFilesize - progress);

	do
	{
		iBuffLength = (m_iFilesize - progress > _sharedFileIOBufferCapa) ? _sharedFileIOBufferCapa : (m_iFilesize - progress);
		if (!::WriteFile(m_hFile, stream + progress, iBuffLength, nullptr, nullptr))
			return false;

		progress += iBuffLength;

		int iPerc = static_cast<int>(static_cast<float>(static_cast<float>(progress) / m_iFilesize) * 100);
		if (iPerc != m_percent)
		{
			m_percent = iPerc;
			_printUtil::Print(std::to_string(iPerc) + " %");
		}
	} while (progress < m_iFilesize);

	return true;
}

bool SharedFile::runInfoCallback(sharedFileInfoCbType callback)
{
	if (callback == nullptr)
		return false;

	std::shared_ptr<sharedFileCbData> data = std::make_shared<sharedFileCbData>();

	data->m_dataFilename = m_filename;
	data->m_iDataFilesize = m_iFilesize;
	callback(data);
	return true;
}

bool SharedFile::DoMapping()
{
	if (!mapping())
	{
		deinitialize();
		return false;
	}
	//showMapped();
	return true;
}

bool SharedFile::FetchFile()
{
	auto passed = [](bool cond, bool& res)
	{ res = cond; return cond; };
	bool pResult = false;

	do
	{
		if (!passed(loadMapping(), pResult))
			break;
		if (!passed(writeFromMemory(), pResult))
			break;
	} while (false);

	reset();
	deinitialize();
	if (pResult)
		runInfoCallback(m_onCreated);

	return pResult;
}

void SharedFile::RegistAction(sharedFileInfoCbType infocb, callbackActions action)
{
	if (infocb == nullptr)
		return;

	switch (action)
	{
	case callbackActions::OnCreated:
		m_onCreated = infocb;
		break;
	case callbackActions::OnMapped:
		m_onMapped = infocb;
		break;
	case callbackActions::None:
	default:
		return;
	}
}

