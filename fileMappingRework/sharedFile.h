
#ifndef SHARED_FILE_H__
#define SHARED_FILE_H__

#include <windows.h>
#include <string>
#include <memory>
#include <list>
#include <vector>
#include <functional>

constexpr uint32_t _sharedFileNameLength = 100;

using sharedFileHeaderType = struct _sharedFileHeaderType
{
	uint32_t m_iMagic;
	uint32_t m_iHeaderLength;
	uint32_t m_iStreamLength;
	char m_szFilename[_sharedFileNameLength];
};

using sharedFileCbData = struct _sharedFileCbData
{
	uint32_t m_iDataFilesize;
	std::string m_dataFilename;
};

class SharedFile
{
public:
	using sharedFileInfoCbType = std::function<void(std::shared_ptr<sharedFileCbData>)>;
	enum class callbackActions
	{
		None,
		OnCreated,
		OnMapped
	};

private:
	using sharedFileDeinitType = bool (SharedFile::*)(void);

private:
	HANDLE m_hFile;
	HANDLE m_hMapping;
	std::string m_filename;
	uint32_t m_iFilesize;
	std::unique_ptr<sharedFileHeaderType> m_header;
	std::list<sharedFileDeinitType> m_deinitList;
	byte* m_pMappedPtr;
	std::vector<byte> m_filestream;

	std::string m_sharedmemId;
	sharedFileInfoCbType m_onMapped;
	sharedFileInfoCbType m_onCreated;

	std::list<std::vector<byte>> m_streamqueue;

public:
	explicit SharedFile(const std::string& filename = {});
	~SharedFile();

private:
	bool isCompleted(bool bCond, sharedFileDeinitType deinit);
	bool deinitialize();
	bool create(uint32_t accessmode, uint32_t dwCreateFlag = 0);
	inline bool isValidFileHandle() const;
	bool reset();
	bool filesize();
	std::string getOnlyFilename();
	inline bool mappingSize(uint32_t& iMappingSize);
	bool makeHeader();
	bool writeHeader();
	bool writeStream();

	bool unsetFilesize();
	bool deinitMapHandle();
	bool unmapping();
	bool readFromFile(uint32_t& progress);
	bool dumping(uint32_t &progress);
	bool mapping();

	void showMapped();
	bool loadMapping();
	bool checkHeader();
	bool writeFromMemory();

	bool runInfoCallback(sharedFileInfoCbType callback);

public:
	bool DoMapping();
	bool FetchFile();
	void RegistAction(sharedFileInfoCbType infocb, callbackActions action = callbackActions::None);

private:
	uint32_t m_percent;
};

bool SharedFile::isValidFileHandle() const
{
	return m_hFile != INVALID_HANDLE_VALUE;
}

bool SharedFile::mappingSize(uint32_t& iMappingSize)
{
	if (!m_header)
		return false;

	iMappingSize = m_header->m_iHeaderLength + m_header->m_iStreamLength;
	return true;

}

#endif